from django.shortcuts import render
from API.constants import END_POINTS
from django.http import JsonResponse
import requests
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response


def get_consortium_providers_list(consortium_id):
    page_number = 1
    providers = []
    while True:
        consortium_providers_api = END_POINTS["PROVIDERS"] + "?consortium-id=" + \
            consortium_id + "&page[number]=" + \
            str(page_number) + "&page[size]=100"
        providers_response = requests.get(consortium_providers_api)
        result = providers_response.json()
        providers += result["data"]
        page_number += 1
        if not "next" in result["links"]:
            break
    return providers

def get_consortium_providers_data(consortium_id):
    providers = get_consortium_providers_list(consortium_id)
    for provider in providers:
        provider_doi_stats_api = END_POINTS["CLIENTS_PRODUCTION_STATS"] + "?provider-id=" + \
            provider["id"]
        stats_response = requests.get(provider_doi_stats_api)
        clients = stats_response.json()
        for client in clients:
            clients_sector = provider["relationships"]["clients"]["data"]
            matched_client = next(
                (c for c in clients_sector if c["id"] == client["id"]), None)
            matched_client.update(client)

    return providers


class ConsortiumStatsMinified(APIView):
    def get(self, request, consortium_id):
        result = {}
        providers = get_consortium_providers_data(consortium_id)
        for provider in providers:
            result.update({
                provider["id"]:
                {client["id"]: client["count"] if "count" in client else None
                 for client in provider["relationships"]["clients"]["data"]}
            })
        return Response(result, status=status.HTTP_200_OK)


class ConsortiumStats(APIView):
    def get(self, request, consortium_id):
        result = []
        providers = get_consortium_providers_data(consortium_id)
        for provider in providers:
            result.append({
                "id": provider["id"],
                "name": provider["attributes"]["name"],
                "clients": [
                    {
                        "id": client["id"],
                        "title": client["id"],
                        "count": client["count"] if "count" in client else None,
                        "temporal": client["temporal"] if "temporal" in client else None,
                        "states": client["states"] if "states" in client else None,
                    } for client in provider["relationships"]["clients"]["data"]
                ]
            })
        return Response(result, status=status.HTTP_200_OK)


class ConsortiumStatsDetailed(APIView):
    def get(self, request, consortium_id):
        providers = get_consortium_providers_data(consortium_id)
        return Response(providers, status=status.HTTP_200_OK)
