from django.urls import path
from API.views import ConsortiumStatsMinified, ConsortiumStats, ConsortiumStatsDetailed

urlpatterns = [
    path('consortium_stats/minified/<str:consortium_id>',
         ConsortiumStatsMinified.as_view()),
    path('consortium_stats/<str:consortium_id>', ConsortiumStats.as_view()),
    path('consortium_stats/detailed/<str:consortium_id>',
         ConsortiumStatsDetailed.as_view()),
]
