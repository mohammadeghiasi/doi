from django.test import TestCase
from API.views import get_consortium_providers_list

class ConsortiumTestCase(TestCase):
    def test_number_of_tibco_providers(self):
        providers_list = get_consortium_providers_list("tibco")
        self.assertEqual(len(providers_list), 138)