## Getting started

To enable the environment (Linux and Windows):

```plaintext
 (Linux) source venv/bin/activate
```

```plaintext
 (Windows) venv/scripts/activate
```

> _**(Windows) FixError:**_ if you are using the Windows powershell to run the commands (instead of cmd), there may be a need to also run the following command in order to provide the required permissions.
> 
> ```plaintext
> Set-ExecutionPolicy Unrestricted -Scope Process
> ```

install Python dependencies:

```plaintext
pip install -r requirements.txt
```

Now you can run the tests:

```plaintext
python DOI/manage.py test
```

or run the Django server and then you have access to the APIs through your browser:

```plaintext
python DOI/manage.py runserver
```